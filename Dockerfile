FROM rust:latest as builder

ADD . /osbyexample
WORKDIR /osbyexample

# Building server
RUN cargo build --release

# Download base image
FROM debian:bullseye

EXPOSE 9975
COPY --from=builder /osbyexample/target/release/httpd /usr/local/osbyexample/
RUN chmod 0755 /usr/local/osbyexample/httpd

ENTRYPOINT ["/usr/local/osbyexample/httpd"]
